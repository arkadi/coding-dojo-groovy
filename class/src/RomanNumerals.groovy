class RomanNumerals {

    def romanDigits = [
            1: 'I',
            5: 'V',
            10: 'X',
            50: 'L',
            100: 'C',
            500: 'D',
            1000: 'M',

            4: 'IV',
            9: 'IX',
            40: 'XL',
            90: 'XC',
            400: 'CD',
            900: 'CM'
    ]

    def reverseRomanKeys = romanDigits.keySet().sort().reverse()

    def arabicDigits = romanDigits.inject([:]) {map, entry ->
        map[entry.value] = entry.key
        map
    }

    def toRoman(arabic) {
        def s = ''
        reverseRomanKeys.every {key ->
            while (arabic >= key) {
                arabic = arabic - key
                s += romanDigits[key]
            }
            arabic
        }
        s
    }

    def toArabic(String roman) {
        def n = 0
        while (roman.length()) {
            for (i in 1..0) {
                if (roman.length() > i && arabicDigits.containsKey(roman[0..i])) {
                    n += arabicDigits[roman[0..i]]
                    roman = roman.substring(1 + i)
                    break
                }
            }
        }
        n
    }
}
