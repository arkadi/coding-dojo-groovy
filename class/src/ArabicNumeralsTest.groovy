class ArabicNumeralsTest extends GroovyTestCase {

    def converter = new RomanNumerals()

    void testOne() {
        assert 1 == converter.toArabic("I")
    }

    void testDigit() {
        assert 4 == converter.toArabic("IV")
        assert 5 == converter.toArabic("V")
        assert 9 == converter.toArabic("IX")
        assert 10 == converter.toArabic("X")
        assert 40 == converter.toArabic("XL")
        assert 50 == converter.toArabic("L")
        assert 90 == converter.toArabic("XC")
        assert 100 == converter.toArabic("C")
        assert 400 == converter.toArabic("CD")
        assert 500 == converter.toArabic("D")
        assert 900 == converter.toArabic("CM")
        assert 1000 == converter.toArabic("M")
    }

    void test19() {
        assert 19 == converter.toArabic("XIX")
    }

    void test999() {
        assert 999 == converter.toArabic("CMXCIX")
    }

    void test99() {
        assert 99 == converter.toArabic("XCIX")
    }

    void test1789() {
        assert 1789 == converter.toArabic("MDCCLXXXIX")
    }

    void testIdiot() {
        assert 0 == converter.toArabic("")
    }

}
