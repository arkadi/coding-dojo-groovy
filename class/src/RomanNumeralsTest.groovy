class RomanNumeralsTest extends GroovyTestCase {

    def converter = new RomanNumerals()

    void testOne() {
        assert "I" == converter.toRoman(1)
    }

    void testFive() {
        assert "V" == converter.toRoman(5)
    }

    void testDigits() {
        assert "X" == converter.toRoman(10)
        assert "L" == converter.toRoman(50)
        assert "C" == converter.toRoman(100)
        assert "D" == converter.toRoman(500)
        assert "M" == converter.toRoman(1000)
    }

    void testTwoDigits() {
        assert 'VI' == converter.toRoman(6)
        assert 'XI' == converter.toRoman(11)
    }

    void testTwoSimilarDigits() {
        assert 'II' == converter.toRoman(2)
    }

    void testThreeSimilarDigits() {
        assert 'III' == converter.toRoman(3)
    }

    void testSeven() {
        assert 'VII' == converter.toRoman(7)
    }

    void test4() {
        assert 'IV' == converter.toRoman(4)
    }

    void test40() {
        assert 'XL' == converter.toRoman(40)
    }

    void test9() {
        assert 'IX' == converter.toRoman(9)
    }

    void testFours() {
        assert 'CD' == converter.toRoman(400)
        assert 'CDXL' == converter.toRoman(440)
        assert 'CDXLIV' == converter.toRoman(444)
    }

    void testNines() {
        assert 'CM' == converter.toRoman(900)
        assert 'CMXC' == converter.toRoman(990)
        assert 'CMXCIX' == converter.toRoman(999)
    }

    void test1789() {
        assert 'MDCCLXXXIX' == converter.toRoman(1789)
    }


    void test0() {
        assert '' == converter.toRoman(0)
    }

}
