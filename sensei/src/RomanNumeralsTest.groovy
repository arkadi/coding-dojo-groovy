
class RomanNumeralsTest extends GroovyTestCase {

    def converter = new RomanNumerals()

    void testOne() {
        assert "I" == converter.toRoman(1)
    }

    void testTwo() {
        assert "II" == converter.toRoman(2)
    }

    void testNineteen() {
        assert "XIX" == converter.toRoman(19)
    }

    void test1666() {
        assert "MDCLXVI" == converter.toRoman(1666)
    }

    void test1789() {
        assert "MDCCLXXXIX" == converter.toRoman(1789)
    }

    void test1990() {
        assert "MCMXC" == converter.toRoman(1990)
    }

    void test2008() {
        assert "MMVIII" == converter.toRoman(2008)
    }

    void test4k() {
        assert "MMMM" == converter.toRoman(4000)
    }

    void testZero() {
        assert "" == converter.toRoman(0)
    }
}
