
class RomanNumerals {

    def romans = new TreeMap(Collections.reverseOrder())

    RomanNumerals() {
        romans.putAll(
          [ 1: 'I', 4: "IV", 5: "V", 9: "IX", 10: "X", 40: "XL", 50: "L", 90: "XC",
            100: "C", 400: "CD", 500: "D", 900: "CM", 1000: "M" ]
        )
    }

    String toRoman(int arabic) {
        def result = ""
        romans.every { arabicChunk, romanDigit ->
            while (arabic >= arabicChunk) {
                arabic -= arabicChunk
                result += romanDigit
            }
            arabic
        }
        result
    }
}
